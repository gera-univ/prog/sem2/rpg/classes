﻿namespace RoleGameEntities
{
    public abstract class Artifact : IMagick
    {
        public Artifact(bool isDisposable)
        {
            IsDisposable = isDisposable;
        }
        
        public virtual bool IsDisposable { get; set; }
        
        public abstract void Affect(Character target);
    }
}