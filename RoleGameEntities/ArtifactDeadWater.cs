﻿namespace RoleGameEntities
{
    public class ArtifactDeadWater : BottleArtifact
    {
        public ArtifactDeadWater(Bottles size, bool isDisposable = true) : base(size, isDisposable)
        {
        }

        public override void Affect(Character target)
        {
            if (target is Wizard w)
                w.Magicka += Power;
        }
    }
}
