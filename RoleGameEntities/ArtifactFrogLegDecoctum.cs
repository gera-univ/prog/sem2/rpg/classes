﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoleGameEntities
{
    public class ArtifactFrogLegDecoctum : Artifact
    {
        public ArtifactFrogLegDecoctum(bool isDisposable = true) : base(isDisposable)
        { 
        }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Poisoned)
                target.Condition = CharacterCondition.Normal;
            target.UpdateCondition();    
            
        }
    }
}
