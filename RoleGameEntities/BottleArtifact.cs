﻿namespace RoleGameEntities
{
    public abstract class BottleArtifact : Artifact
    {
        public enum Bottles 
        {
            small = 10,
            middle = 25,
            big = 50
        }

        protected BottleArtifact(Bottles size, bool isDisposable) : base(isDisposable)
        {
            Power = (int)size;
        }

        public int Power { get; set; }
    }
}
