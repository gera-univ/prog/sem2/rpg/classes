﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace RoleGameEntities
{
    public class Character : IComparable
    {
        private static int _idCounter;

        private readonly List<Artifact> _inventory = new List<Artifact>();
        private int _health;
        private bool _isMaxHealthSet;
        private int _maxHealth;

        public Character(string name, CharacterRace race, CharacterGender gender)
        {
            Id = Interlocked.Increment(ref _idCounter);
            Name = name;
            Race = race;
            Gender = gender;
        }

        public int Id { get; }
        public string Name { get; }
        public CharacterCondition Condition { get; set; }
        public bool CanTalk { get; set; } = true;
        public bool CanMove { get; set; } = true;
        public bool IsInvincible { get; set; }
        public CharacterRace Race { get; }
        public CharacterGender Gender { get; }
        public int Age { get; set; }
        public int NActiveArtifact { get; private set; }

        public int Health
        {
            get => _health;
            set
            {
                if (!IsInvincible)
                {
                    _health = value;
                    if (!_isMaxHealthSet)
                        MaxHealth = _health;
                }

                UpdateCondition();
            }
        }

        public int MaxHealth
        {
            get => _maxHealth;
            set
            {
                _maxHealth = value;
                _isMaxHealthSet = true;
            }
        }

        public int Experience { get; set; }

        public int CompareTo(object other)
        {
            try
            {
                var otherCharacter = (Character) other;
                return Experience.CompareTo(otherCharacter.Experience);
            }
            catch (Exception)
            {
                throw new ArgumentException();
            }
        }

        public void PickupArtifact(Artifact a)
        {
            _inventory.Add(a);
        }

        public bool AbandonArtifact()
        {
            if (_inventory.Count == 0)
                return false;
            _inventory.RemoveAt(NActiveArtifact);
            UpdateCondition();
            return true;
        }

        public void NextArtifact()
        {
            ++NActiveArtifact;
            UpdateCondition();
        }

        public void PreviousArtifact()
        {
            --NActiveArtifact;
            UpdateCondition();
        }

        public bool PassArtifact(Character target)
        {
            if (_inventory.Count == 0)
                return false;
            target.PickupArtifact(_inventory[NActiveArtifact]);
            _inventory.RemoveAt(NActiveArtifact);
            UpdateCondition();
            return true;
        }

        public bool UseArtifact(Character target)
        {
            if (_inventory.Count == 0 || Condition == CharacterCondition.Dead)
                return false;
            _inventory[NActiveArtifact].Affect(target);
            if (_inventory[NActiveArtifact].IsDisposable)
                _inventory.RemoveAt(NActiveArtifact);
            UpdateCondition();
            return true;
        }

        public virtual void UpdateCondition()
        {
            if (_health > MaxHealth)
                _health = MaxHealth;
            if (_health <= 0)
            {
                _health = 0;
                Condition = CharacterCondition.Dead;
            }
            else if (_health < 10 && Condition == CharacterCondition.Normal)
            {
                Condition = CharacterCondition.Weakened;
            }
            else if (Condition == CharacterCondition.Weakened)
            {
                Condition = CharacterCondition.Normal;
            }
            else if(Condition == CharacterCondition.Paralyzed)
            {
                CanMove = false;
            }
            if (_inventory.Count == 0)
                NActiveArtifact = 0;
            else if (NActiveArtifact >= _inventory.Count)
                NActiveArtifact = 0;
            else if (NActiveArtifact < 0)
                NActiveArtifact = _inventory.Count - 1;
        }
    }
}