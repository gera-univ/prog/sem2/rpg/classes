﻿namespace RoleGameEntities
{
    public enum CharacterCondition
    {
        Normal,
        Weakened,
        Sick,
        Poisoned,
        Paralyzed,
        Dead
    }
}