﻿namespace RoleGameEntities
{
    public interface IMagick
    {         
        public void Affect(Character target);
    }
}