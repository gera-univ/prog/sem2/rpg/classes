﻿namespace RoleGameEntities
{
    public abstract class PoweredArtifact : Artifact
    {
        protected PoweredArtifact(int power, bool isDisposable) : base(isDisposable)
        {
            Power = power;
        }

        public int Power { get; set; }
    }
}