﻿namespace RoleGameEntities
{
    public class SpellCurePoison : Spell
    {
        public SpellCurePoison(int cost = 30, bool isVerbal = false, bool isMotoric = true)
        {
            Cost = cost;
            IsVerbal = isVerbal;
            IsMotoric = isMotoric;
        }

        public override void Affect(Character target)
        {
            if (target.Condition == CharacterCondition.Poisoned)
                target.Condition = CharacterCondition.Normal;
            target.UpdateCondition();
        }
    }
}