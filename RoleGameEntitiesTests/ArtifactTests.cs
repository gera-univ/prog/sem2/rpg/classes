﻿using NUnit.Framework;
using RoleGameEntities;

namespace RoleGameClassesTests
{
    [TestFixture]
    public class ArtifactTests
    {
        [Test]
        public void LivingWater_HealsCharacter()
        {
            var c = DummyFactory.GetCharacter(health: 10, maxHealth: 15);

            var water = new ArtifactLivingWater(BottleArtifact.Bottles.small);
            water.Affect(c);

            Assert.AreEqual(15, c.Health);
        }

        [Test]
        public void DeadWater_IncreasesWizardsMagicka()
        {
            var c = DummyFactory.GetWizard(magicka:20, maxMagicka:35);

            var water = new ArtifactDeadWater(BottleArtifact.Bottles.middle);
            water.Affect(c);

            Assert.AreEqual(35, c.Magicka);
        }

        [Test]
        public void BasiliskEye_WhenTargetIsntDead_MakesItParalyzed()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Normal;

            var basiliskEye = new ArtifactBasiliskEye();
            basiliskEye.Affect(c);

            Assert.AreEqual(CharacterCondition.Paralyzed, c.Condition);
        }

        [Test]
        public void FrogLegDecoctum_WhenTargetIsPoisoned_CuresItToNormal()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Poisoned;

            var frogLegDecoctum = new ArtifactFrogLegDecoctum();
            frogLegDecoctum.Affect(c);

            Assert.AreEqual(CharacterCondition.Normal, c.Condition);
        }

        [Test]
        public void PoisonousSaliva_WhenTargetIsNormalOrWeakened_MakesItPoisoned()
        {
            var c = DummyFactory.GetCharacter(health: 10);
            c.Condition = CharacterCondition.Normal;

            var poisonousSaliva = new ArtifactPoisonousSaliva(15);
            poisonousSaliva.Affect(c);

            Assert.AreEqual(CharacterCondition.Dead, c.Condition);
        }

        [Test]
        public void PoisonousSaliva_WhenTargetIsNormalOrWeakened_MakesItPoisonedOrKillsIt()
        {
            var c = DummyFactory.GetCharacter(health: 10);
            c.Condition = CharacterCondition.Normal;

            var poisonousSaliva = new ArtifactPoisonousSaliva(5);
            poisonousSaliva.Affect(c);

            Assert.AreEqual(CharacterCondition.Poisoned, c.Condition);
        }

        [Test]
        public void CrookLightning_TakesCharacterHealth()
        {
            var c = DummyFactory.GetCharacter(health: 10);
            var crookLightning = new ArtifactCrookLightning(20);
            crookLightning.Affect(c);

            Assert.AreEqual(0, c.Health);
        }
    }
}