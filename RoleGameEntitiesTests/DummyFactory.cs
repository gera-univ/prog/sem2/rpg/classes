﻿using RoleGameEntities;

namespace RoleGameClassesTests
{
    public static class DummyFactory
    {
        public const string DefaultName = "dummy";
        public const CharacterRace DefaultRace = CharacterRace.Dummy;
        public const CharacterGender DefaultGender = CharacterGender.Male;
        public const int DefaultMaxHealth = 100;
        public const int DefaultHealth = 90;

        public static Character GetCharacter(string name = DefaultName, CharacterRace race = DefaultRace,
            CharacterGender gender = DefaultGender, int health = DefaultHealth, int maxHealth = DefaultMaxHealth)
        {
            return new Character(name, race, gender) {Health = health, MaxHealth = maxHealth};
        }

        public const int DefaultMagicka = 90;
        public const int DefaultMaxMagicka = 10;

        public static Wizard GetWizard(string name = DefaultName, CharacterRace race = DefaultRace,
            CharacterGender gender = DefaultGender, int health = DefaultHealth, int maxHealth = DefaultMaxHealth,
            int magicka = DefaultMagicka, int maxMagicka = DefaultMaxMagicka)
        {
            return new Wizard(name, race, gender)
            {
                Health = health, MaxHealth = maxHealth,
                Magicka = magicka, MaxMagicka = maxMagicka
            };
        }
    }
}