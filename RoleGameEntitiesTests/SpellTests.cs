﻿using System.Threading;
using NUnit.Framework;
using RoleGameEntities;

namespace RoleGameClassesTests
{
    [TestFixture]
    public class SpellTests
    {
        [Test]
        public void Armor_MakesCharacterInvincible()
        {
            var c = DummyFactory.GetCharacter();

            var armor = new SpellArmor(100);

            armor.Affect(c);

            c.Health -= DummyFactory.DefaultMaxHealth;

            Assert.AreEqual(DummyFactory.DefaultHealth, c.Health);
            Assert.AreEqual(CharacterCondition.Normal, c.Condition);

            armor.Disaffect(c);

            c.Health -= DummyFactory.DefaultMaxHealth;

            Assert.AreEqual(CharacterCondition.Dead, c.Condition);
        }

        [Test]
        public void Armor_WhenExpired_DisaffectsCharacter()
        {
            var c = DummyFactory.GetCharacter();

            var armor = new SpellArmor(100);

            armor.Affect(c);

            c.Health -= DummyFactory.DefaultMaxHealth;

            Assert.AreEqual(DummyFactory.DefaultHealth, c.Health);

            Thread.Sleep(200);

            c.Health -= DummyFactory.DefaultMaxHealth;

            Assert.AreNotEqual(DummyFactory.DefaultHealth, c.Health);
        }

        [Test]
        public void CureDisease_WhenTargetIsSick_CuresItToNormal()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Sick;
            var cureDisease = new SpellCureDisease();
            cureDisease.Affect(c);

            Assert.AreEqual(CharacterCondition.Normal, c.Condition);
        }

        [Test]
        public void CurePoison_WhenTargetIsPoisoned_CuresItToNormal()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Poisoned;
            var curePoison = new SpellCurePoison();
            curePoison.Affect(c);

            Assert.AreEqual(CharacterCondition.Normal, c.Condition);
        }

        [Test]
        public void Heal_HealsCharacter()
        {
            var c = DummyFactory.GetCharacter(health: 10, maxHealth: 15);

            var heal = new SpellHeal(10);
            heal.Affect(c);

            Assert.AreEqual(15, c.Health);
        }

        [Test]
        public void Release_WhenTargetIsParalyzed_CuresItToNormal()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Paralyzed;
            var release = new SpellRelease();
            release.Affect(c);

            Assert.AreEqual(CharacterCondition.Normal, c.Condition);
        }

        [Test]
        public void Resurrect_WhenTargetIsDead_CuresItToNormal()
        {
            var c = DummyFactory.GetCharacter();
            c.Condition = CharacterCondition.Dead;
            var resurrect = new SpellResurrect();
            resurrect.Affect(c);

            Assert.AreEqual(CharacterCondition.Normal, c.Condition);
        }
    }
}