﻿using NUnit.Framework;
using RoleGameEntities;

namespace RoleGameClassesTests
{
    [TestFixture]
    public class WizardTests
    {
        private static Wizard InitializeWizardWith3Spells()
        {
            var wizz = DummyFactory.GetWizard();
            wizz.LearnSpell(new SpellHeal(10));
            wizz.LearnSpell(new SpellCurePoison());
            wizz.LearnSpell(new SpellCureDisease());
            return wizz;
        }

        [Test]
        public void Cast_WhenCastingMotoricSpellAndWizardCantMove_ReturnsFalse()
        {
            var wizz = DummyFactory.GetWizard();
            wizz.LearnSpell(new SpellCureDisease());
            wizz.CanMove = false;
            Assert.IsFalse(wizz.Cast(DummyFactory.GetCharacter()));
        }

        [Test]
        public void Cast_WhenCastingVerbalSpellAndWizardCantTalk_ReturnsFalse()
        {
            var wizz = DummyFactory.GetWizard();
            wizz.LearnSpell(new SpellRelease());
            wizz.CanTalk = false;
            Assert.IsFalse(wizz.Cast(DummyFactory.GetCharacter()));
        }

        [Test]
        public void Cast_WhenNotEnoughMagicka_ReturnsFalse()
        {
            var wizz = DummyFactory.GetWizard(magicka: 20);
            wizz.LearnSpell(new SpellHeal(10));
            var target = DummyFactory.GetCharacter();
            Assert.IsTrue(wizz.Cast(target));
            Assert.IsFalse(wizz.Cast(target));
        }

        [Test]
        public void ForgetSpell_WhenNoSpellsLeft_ReturnsFalse()
        {
            var wizz = InitializeWizardWith3Spells();
            Assert.IsTrue(wizz.ForgetSpell());
            Assert.IsTrue(wizz.ForgetSpell());
            Assert.IsTrue(wizz.ForgetSpell());
            Assert.IsFalse(wizz.ForgetSpell());
        }

        [Test]
        public void NCurrentSpell_WhenSpellForgotten_StaysTheSame()
        {
            var wizz = InitializeWizardWith3Spells();
            Assert.AreEqual(0, wizz.NActiveSpell);
            wizz.ForgetSpell();
            Assert.AreEqual(0, wizz.NActiveSpell);
        }

        [Test]
        public void Wizard_LoopsThroughSpellsBackward()
        {
            var wizz = InitializeWizardWith3Spells();

            Assert.AreEqual(0, wizz.NActiveSpell);
            wizz.PreviousSpell();
            Assert.AreEqual(2, wizz.NActiveSpell);
            wizz.PreviousSpell();
            Assert.AreEqual(1, wizz.NActiveSpell);
            wizz.PreviousSpell();
            Assert.AreEqual(0, wizz.NActiveSpell);
        }

        [Test]
        public void Wizard_LoopsThroughSpellsForward()
        {
            var wizz = InitializeWizardWith3Spells();

            Assert.AreEqual(0, wizz.NActiveSpell);
            wizz.NextSpell();
            Assert.AreEqual(1, wizz.NActiveSpell);
            wizz.NextSpell();
            Assert.AreEqual(2, wizz.NActiveSpell);
            wizz.NextSpell();
            Assert.AreEqual(0, wizz.NActiveSpell);
        }

        [Test]
        public void Wizard_WhenMagickaExceedsMaxMagicka_CapsToMaxMagicka()
        {
            var wizz = DummyFactory.GetWizard(magicka: 10, maxMagicka: 20);
            wizz.Magicka += 30;
            Assert.AreEqual(20, wizz.Magicka);
        }

        [Test]
        public void Wizard_WhenMagickaSubceedsZero_SetsToZero()
        {
            var wizz = DummyFactory.GetWizard(magicka: 10);
            wizz.Magicka -= 30;
            Assert.AreEqual(0, wizz.Magicka);
        }

        [Test]
        public void CastSpell_WhenWizardIsDead_ReturnsFalse()
        {
            var c = DummyFactory.GetWizard();
            c.LearnSpell(new SpellResurrect());
            c.Condition = CharacterCondition.Dead;
            Assert.IsFalse(c.Cast(c));
        }
    }
}